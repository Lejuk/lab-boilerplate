export function modifyUsersFromMock(randomUserMock) {
  randomUserMock = randomUserMock.map((person) => ({
    gender: person.gender,
    title: person.name.title,
    full_name: `${person.name.first}` + ' ' + `${person.name.last}`,
    city: person.location.city,
    state: person.location.state,
    country: person.location.country,
    postcode: person.location.postcode,
    coordinates: person.location.coordinates,
    timezone: person.location.timezone,
    email: person.email,
    b_date: person.dob.date,
    age: person.dob.age,
    phone: person.phone,
    picture_large: person.picture.large,
    picture_thumbnail: person.picture.thumbnail,
    id: Math.random(randomUserMock.length),
    favorite: person.gender === 'female',
    course: Math.random(4),
    bg_color: 'Red',
    note: 'Some Note',
  }));
  return randomUserMock;
}

export function combineTwoArrays(random_user_mock, additional_users) {
  additional_users.forEach((el) => {
    if (!random_user_mock.find((obj) => el.full_name === obj.full_name)) { random_user_mock.push(el); }
  });
  return random_user_mock;
}
