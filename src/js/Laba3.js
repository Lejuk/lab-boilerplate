import { randomUserMock, additionalUsers } from './mock_for_L3.js';
import { modifyUsersFromMock, combineTwoArrays } from './Task1.js';
import { filterArray } from './Task3.js';
import { validate } from './Task2.js';
import { sortArray } from './Task4.js';
import { findParticularUser } from './Task5.js';
import { percentageOf } from './Task6.js';

// task1
const modified = modifyUsersFromMock(randomUserMock);
modified.forEach((element) => console.log(element));

const modifiedCombined = combineTwoArrays(modified, additionalUsers);
// modifiedCombined.forEach(element => console.log(element));

// task2

// for(let i =0; i<15; i++)
//     validate(modifiedCombined[i]);

// task3
const filteredUsers = filterArray(modifiedCombined, 'country', 'Germany');
// filteredUsers.forEach(element => console.log(element));

// task4
const newUsers = sortArray(modifiedCombined, 'age', false);
// newUsers.forEach(element => console.log(element));

// task5
const particUser = findParticularUser(modifiedCombined, 'age', 24);
// console.log(particUser);

// task6
// console.log(percentageOf(modifiedCombined, 99, 'less'));
