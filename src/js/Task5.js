export function findParticularUser(array, field, param) {
  let particularUser;
  if (typeof (param) === 'string') {
    particularUser = array.find((user) => user[field].toString().toLowerCase().includes(param.toString().toLowerCase()));
  } else { particularUser = array.find((user) => user[field] === param); }

  if (particularUser) { 
    return particularUser; 
  }
  console.log('There is no such an object');
}
