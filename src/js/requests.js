export function getData(url) {
  return fetch(url, {
    method: 'GET',
  })
}

export const postTeacher = (user) => fetch('//localhost:3000/newTeachers', {
  method: 'POST',
  mode: 'cors',
  cache: 'no-cache',
  credentials: 'same-origin',
  headers: {
    'Content-Type': 'application/json',
  },
  redirect: 'follow',
  referrerPolicy: 'no-referrer',
  body: JSON.stringify(user),
}).then((response) => response.json());
