// const testModules = require('./test-module');
import { randomUserMock, additionalUsers } from './mock_for_L3.js';
import { modifyUsersFromMock, combineTwoArrays } from './Task1.js';
import { filterArray } from './Task3.js';
import { validate } from './Task2.js';
import { sortArray } from './Task4.js';
import { findParticularUser } from './Task5.js';
import { percentageOf } from './Task6.js';
import { getData} from './requests.js';
import { postTeacher} from './requests.js';

require('../css/app.css');



const usersFromRequest = getData('https://randomuser.me/api/?results=50&seed=10').then((response)=>response.json()).then((users)=> users.results);


const lab5 = () => {
  usersFromRequest.then((a) => {
    let modified = modifyUsersFromMock(a);
    console.log(modified);
    addTeacher(modified);
    displayTopTeachers(topTeachers, modified);
    displayStatisticsTable(tableBody, modified);
    const favTeachers = modified.filter((element) => element.favorite === true);
    displayFavoriteTeachers(favoriteTeachers, favTeachers);
    addToFavorites(modified);
    getOnlyFavorites(onlyFavs, a);
    getOnlyMales(onlyMales, a);
    getOnlyFromEU(onlyMyCountry, a);
    getOnlyExperienced(onlyExperienced, a);
    sortByName(tableBody, modified);
    sortByAge(tableBody, modified); 
    sortByCountryName(tableBody, modified);
    findUser(topTeachers,modified);
    pagination(topTeachers, modified);
  })
};

lab5();


// task1
let modified = modifyUsersFromMock(randomUserMock);

const modifiedCombined = combineTwoArrays(modified, additionalUsers);

// modifiedCombined.forEach(element => console.log(element));

// task2

// for(let i =0; i<15; i++)
//     validate(modifiedCombined[i]);

// task3
const filteredUsers = filterArray(modifiedCombined, 'country', 'Germany');
// filteredUsers.forEach(element => console.log(element));

// task4
// let newUsers = sortArray(modifiedCombined, 'age', false);
// newUsers.forEach(element => console.log(element));

// task5
const particUser = findParticularUser(modifiedCombined, 'age', 24);
// console.log(particUser);

// task6
// console.log(percentageOf(modifiedCombined, 99, 'less'));

// modified = filterArray(modifiedCombined, 'country', 'United States');

// var modified = modifyUsersFromMock(randomUserMock);


modified = combineTwoArrays(modified, additionalUsers);

const topTeachers = document.querySelector('.wrapper');

function printUsers(topTeachers, modified) {
  topTeachers.innerHTML='';
  let size = modified.length;
  if(modified.length > 10){
    size=10;
  }
  for (let i = 0; i < size; i++) {
    let newItem = document.createElement('figure');    
    newItem.innerHTML = `
        <a href="#popup${modified[i].full_name}">
        <div class="scale">
            <img class="circular--portrait" 
            src=${modified[i].picture_large} 
            alt=${modified[i].full_name}+'Image' 
            width="90" 
            height="90">
        </div>
        <p>${modified[i].full_name}</p><br>
        <p class="country-name">${modified[i].country}</p>
        </a>`;
    if (modified[i].favorite === true) {
      const favIcon = document.createElement('img');
      favIcon.src = 'images/favIcon.png';
      favIcon.classList.add('favoriteIcon');
      const temp = newItem.querySelector('.scale');
      temp.append(favIcon);
    }
    topTeachers.append(newItem);
  }
}

function printUsersOptional(topTeachers, modified, pageValue) {
  topTeachers.innerHTML = '';
  for (let i = (pageValue-1)*10; i < pageValue*10; i++) {
    // console.log('pagevalue is',  i);
    let newItem = document.createElement('figure');
    newItem.innerHTML = `
        <a href="#popup${modified[i].full_name}">
        <div class="scale">
            <img class="circular--portrait" 
            src=${modified[i].picture_large} 
            alt=${modified[i].full_name}+'Image' 
            width="90" 
            height="90">
        </div>
        <p>${modified[i].full_name}</p><br>
        <p class="country-name">${modified[i].country}</p>
        </a>`;
    if (modified[i].favorite === true) {
      const favIcon = document.createElement('img');
      favIcon.src = 'images/favIcon.png';
      favIcon.classList.add('favoriteIcon');
      const temp = newItem.querySelector('.scale');
      temp.append(favIcon);
    }
    topTeachers.append(newItem);
  }
}



function displayTopTeachers(topTeachers, modified) {
  printUsers(topTeachers, modified);

  const afterHeaderNavBar = document.querySelectorAll('.after-header-navbar');

  for (let i = 0; i < modified.length; i++) {
    const teacherPop = document.createElement('div');
    teacherPop.id = `popup${modified[i].full_name}`;
    teacherPop.id = teacherPop.id.replace(' ', '%20');
    teacherPop.classList.add('overlay');
    teacherPop.innerHTML = `
    <div class="popup">
    <h2 class="popup-header">Teacher Info</h2>
    <a class="close" href="#">&times;</a>
    <div class="wrapper2">
        <div>
            <img src=${modified[i].picture_large} 
                 alt=${modified[i].full_name}+'Image' 
                 width="200" 
                 height="200" 
                 class="popup-image">
        </div>
        <div class="wrapper2">
            <h3 class="popup-name">${modified[i].full_name}
                <img src="images/favIconShallow.png" 
                     alt="FavIconShallow" 
                     class="fav-icon-shallow">
            </h3><br>
            <label class="popup-label">${modified[i].country}, ${modified[i].city}</label><br>
            <label class="popup-label">${modified[i].age} ${modified[i].gender}</label><br>
            <label class="popup-email-label">${modified[i].email}</label><br>
            <label class="popup-label">${modified[i].phone}</label>
        </div>
    </div>

    <div class="content">
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima, recusandae reprehenderit eos
        corporis
        aperiam beatae officiis ipsum molestiae
        illum nisi iste architecto facere laudantium, sapiente ab ullam cupiditate? Assumenda tempora esse
        amet.
        Sunt atque nobis beatae earum iste laboriosam dicta,
        magnam facere harum explicabo ducimus voluptatem animi sed totam ut.
    </div>
    <div>
        <p class="label-hover toggle-map">toggle map</p>
    </div>
</div>`;
    if (modified[i].favorite === true) {
      const imageSrc = teacherPop.querySelectorAll('img');
      imageSrc[1].src = 'images/favIcon.png';
    }
    afterHeaderNavBar[2].after(teacherPop);
  }

  for (let i = 0; i < 10; i++) {
    if (modified[i].favorite === true) {
      const favIcon = document.createElement('img');
      favIcon.src = 'images/favIcon.png';
      favIcon.classList.add('favoriteIcon');
      const temp = document.querySelectorAll('.scale')[i];
      temp.append(favIcon);
    }
  }
}

const tableBody = document.querySelectorAll('tbody>tr');

function displayStatisticsTable(tableBody, modified) {
  for (let i = 0; i < tableBody.length; i++) {
    tableBody[i].innerHTML = `
    <td>${modified[i].full_name}</td>
    <td>${modified[i].age}</td>
    <td>${modified[i].gender}</td>
    <td>${modified[i].country}</td>
    `;
  }
}

function displayStatisticsTableOptional(tableBody, modified, pageValue) {
  for (let i = (pageValue-1)*10; i < pageValue*10; i++) {
    tableBody[i-(pageValue-1)*10].innerHTML = `
    <td>${modified[i].full_name}</td>
    <td>${modified[i].age}</td>
    <td>${modified[i].gender}</td>
    <td>${modified[i].country}</td>
    `;
  }
}

const favoriteTeachers = document.querySelectorAll('.wrapper')[1];
const favTeachers = modified.filter((element) => element.favorite === true);

function displayFavoriteTeachers(favoriteTeachers, favTeachers) {
  favoriteTeachers.innerHTML = ``;
  for (let i = 0; i < 5; i++) {
    const newFav = document.createElement('figure');
    newFav.innerHTML = `
    <div class="scale">
        <img class="circular--portrait" 
        src=${favTeachers[i].picture_large} 
        alt=${favTeachers[i].full_name}+'Image' 
        width="90" 
        height="90">
    </div>
    <p>${favTeachers[i].full_name}</p><br>
    <p class="country-name">${favTeachers[i].country}</p>`;
    favoriteTeachers.append(newFav);
  }
}

const onlyFavs = document.getElementById('preferenceSelection');

function getOnlyFavorites(onlyFavs, randomUserMock) {
  onlyFavs.addEventListener('change', () => {
    modified = modifyUsersFromMock(randomUserMock);
    let prefValue = onlyFavs.value.toString();
    if(prefValue === 'Favorite')
      modified = modified.filter((element) => element.favorite === true);
    else
      modified = modified.filter((element) => element.favorite === false);
    printUsers(topTeachers, modified);
    displayStatisticsTable(tableBody, modified);
  });
}

const onlyMales = document.getElementById('genderSelection');

function getOnlyMales(onlyMales, randomUserMock) {
  onlyMales.addEventListener('change', () => {
    modified = modifyUsersFromMock(randomUserMock);
    modified = modified.filter((element) => element.gender === onlyMales.value.toString().toLowerCase());
    printUsers(topTeachers, modified);
    displayStatisticsTable(tableBody, modified);
  });
}

const onlyMyCountry = document.getElementById('countrySelection');

function getOnlyFromEU(onlyMyCountry, randomUserMock) {
  onlyMyCountry.addEventListener('change', () => {
    modified = modifyUsersFromMock(randomUserMock);
    modified = modified.filter((element) => element.country === onlyMyCountry.value.toString());
    printUsers(topTeachers, modified);
    displayStatisticsTable(tableBody, modified);
  });
}

const onlyExperienced = document.getElementById('ageSelection');

function getOnlyExperienced(onlyExperienced, randomUserMock) {
  onlyExperienced.addEventListener('change', () => {
    modified = modifyUsersFromMock(randomUserMock);
    if(onlyExperienced.value.toString() === 'Younger than 35'){
      modified = modified.filter((element) => element.age < 35);
      console.log('menshe');
    }
    else if(onlyExperienced.value.toString() === '35 to 50'){
      modified = modified.filter((element) => element.age >= 35 && element.age <50);
    }
    else if(onlyExperienced.value.toString() === 'Older than 50'){
      modified = modified.filter((element) => element.age >=50);
    }
    printUsers(topTeachers, modified);
    displayStatisticsTable(tableBody, modified);
  });
}

function addToFavorites(modified) {
  const fav = document.querySelectorAll('.fav-icon-shallow');

  for (let i = 0; i < modified.length; i++) {
    if (modified[i].favorite == false) {
      fav[i].addEventListener('click', () => {
        fav[i].src = 'images/favIcon.png';
        const favWrapper = document.querySelectorAll('.wrapper');
        const newFav = document.createElement('figure');
        console.log(modified);
        console.log(modified[Math.abs(i - modified.length + 1)]);
        console.log(i);
        newFav.innerHTML = `
        <a href="#popup${modified[Math.abs(i - modified.length + 1)].full_name}">
        <div class="scale">
            <img class="circular--portrait" 
            src=${modified[Math.abs(i - modified.length + 1)].picture_large} 
            alt=${modified[Math.abs(i - modified.length + 1)].full_name}+'Image' 
            width="90" 
            height="90">
        </div>
        <p>${modified[Math.abs(i - modified.length + 1)].full_name}</p><br>
        <p class="country-name">${modified[Math.abs(i - modified.length + 1)].country}</p>
        </a>`;
        favWrapper[1].append(newFav);
      });
    }
  }
}

function sortByName(tableBody,modifiedCombined) {
  const tableName = document.getElementById('tableName');

  tableName.addEventListener('click', () => {
    modified = sortArray(modifiedCombined, 'full_name', false);
    printUsers(topTeachers, modified);
    displayStatisticsTable(tableBody,modified);
  });
}

function sortByAge(tableBody, modifiedCombined) {
  const tableAge = document.getElementById('tableAge');

  tableAge.addEventListener('click', () => {
    modified = sortArray(modifiedCombined, 'age', false);
    printUsers(topTeachers, modified);
    displayStatisticsTable(tableBody, modified);
  });
}

function sortByCountryName(tableBody, modifiedCombined) {
  const tableCountry = document.getElementById('tableCountry');

  tableCountry.addEventListener('click', () => {
    modified = sortArray(modifiedCombined, 'country', false);
    printUsers(topTeachers, modified);
    displayStatisticsTable(tableBody, modified);
  });
}

function findUser(topTeachers, modified) {
  const searchPlaceHolder = document.querySelector('.section-size');

  const selectionForSearch = document.getElementById('selectionForSearch');

  selectionForSearch.addEventListener('change', () => {
    searchPlaceHolder.placeholder = `Enter ${selectionForSearch.value} for search`;
  });

  const searchButton = document.getElementById('searchButtonID');

  searchButton.addEventListener('click', () => {
    const selectionForSearch = document.getElementById('selectionForSearch');
    const searchinParam = selectionForSearch.value.toString().toLowerCase();
    let inputValue = document.getElementById('Name').value;
    if (Number.parseInt(inputValue)) {
      inputValue = Number.parseInt(inputValue);
    }
    const lookedUser = findParticularUser(modified, searchinParam, inputValue);

    if (lookedUser) {
      console.log(lookedUser);
      topTeachers.innerHTML = ' ';

      let newItem = document.createElement('figure');
      newItem.innerHTML = `
            <a href="#popup${lookedUser.full_name}">
            <div class="scale">
                <img class="circular--portrait" 
                src=${lookedUser.picture_large} 
                alt=${lookedUser.full_name}+'Image' 
                width="90" 
                height="90">
            </div>
            <p>${lookedUser.full_name}</p><br>
            <p class="country-name">${lookedUser.country}</p>
            </a>`;
      if (lookedUser.favorite === true) {
        const temp = newItem.querySelector('.scale');
        const favIcon = document.createElement('img');
        favIcon.src = 'images/favIcon.png';
        favIcon.classList.add('favoriteIcon');
        temp.append(favIcon);
      }
      topTeachers.append(newItem);
    } else { alert('There is no such a user'); }
  });
}

function addTeacher(modified) {
  const addTeacherFirst = document.getElementById('addTeacherButton');
  addTeacherFirst.addEventListener('click', () => {
    const overlaying = document.querySelector('.overlay2');
    overlaying.style.display = 'inherit';
  });

  const afterHeaderNavBar = document.querySelectorAll('.after-header-navbar');
  const addNewTeacher = document.getElementById('pop-up-add-button');

  addNewTeacher.addEventListener('click', () => {
    let newTeacherSex;
    const radios = document.getElementsByName('groupS');

    radios.forEach((element) => {
      if (element.checked) { newTeacherSex = element.value; }
    });

    const user = {
      full_name: document.getElementById('newTeacherName').value,
      gender: newTeacherSex,
      title: 'Default title',
      note: 'Default note',
      state: 'Random state',
      city: document.getElementById('newTeacherCity').value,
      country: document.getElementById('newTeacherCountry').value,
      age: Number.parseInt(document.getElementById('newTeacherAge').value),
      phone: document.getElementById('newTeacherPhone').value,
      email: document.getElementById('newTeacherEmail').value,
    };

    if (validate(user)) {
      postTeacher(user).then((response) => console.log('response: ', response));
      modified.unshift(user);
      console.log(modified);
      displayTopTeachers(topTeachers, modified);
      displayStatisticsTable(tableBody, modified);
    //   const wrapper = document.querySelector('.wrapper');
    //   const newTeacher = document.createElement('figure');
    //   newTeacher.innerHTML = `
    //     <a href="#popup${user.full_name}">
    //     <div class="scale">
    //         <img class="circular--portrait" 
    //         src="https://yt3.ggpht.com/ytc/AAUvwng_cj9izCmXl7_umbQNfzdL6EUM4EIsl4RQKXhk=s900-c-k-c0x00ffffff-no-rj" 
    //         alt=${user.full_name}+'Image' 
    //         width="90" 
    //         height="90">
    //     </div>
    //     <p>${user.full_name}</p><br>
    //     <p class="country-name">${user.country}</p>
    //     </a>`;
    //   wrapper.append(newTeacher);

    //   const teacherPop = document.createElement('div');
    //   teacherPop.id = `popup${user.full_name}`;
    //   teacherPop.id = teacherPop.id.replace(' ', '%20');
    //   teacherPop.classList.add('overlay');
    //   teacherPop.innerHTML = `
    //     <div class="popup">
    //         <h2 class="popup-header">Teacher Info</h2>
    //         <a class="close" href="#">&times;</a>
    //         <div class="wrapper2">
    //             <div>
    //                 <img src="https://yt3.ggpht.com/ytc/AAUvwng_cj9izCmXl7_umbQNfzdL6EUM4EIsl4RQKXhk=s900-c-k-c0x00ffffff-no-rj" 
    //                 alt=${user.full_name}+'Image' 
    //                 width="200" 
    //                 height="200" 
    //                 class="popup-image">
    //             </div>
    //     <div class="wrapper2">
    //         <h3 class="popup-name">${user.full_name}
    //             <img src="images/favIconShallow.png" 
    //                  alt="FavIconShallow" 
    //                  class="fav-icon-shallow">
    //         </h3><br>
    //         <label class="popup-label">${user.country}, ${user.city}</label><br>
    //         <label class="popup-label">${user.age} ${user.sex}</label><br>
    //         <label class="popup-email-label">${user.email}</label><br>
    //         <label class="popup-label">${user.phone}</label>
    //     </div>
    // </div>

    // <div class="content">
    //     Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima, recusandae reprehenderit eos
    //     corporis
    //     aperiam beatae officiis ipsum molestiae
    //     illum nisi iste architecto facere laudantium, sapiente ab ullam cupiditate? Assumenda tempora esse
    //     amet.
    //     Sunt atque nobis beatae earum iste laboriosam dicta,
    //     magnam facere harum explicabo ducimus voluptatem animi sed totam ut.
    // </div>
    // <div>
    //     <p class="label-hover toggle-map">toggle map</p>
    // </div>
    // </div>`;
    //   afterHeaderNavBar[2].after(teacherPop);
    //   const overlaying = document.querySelector('.overlay2');
    //   overlaying.style.display = 'none';
      alert(`New teacher ${user.full_name} has been added`);
    }
  });
}

// task1
// displayTopTeachers(topTeachers,modified);
// displayStatisticsTable(tableBody,modified);
// displayFavoriteTeachers(favoriteTeachers);
// addToFavorites();


// task2
// getOnlyFavorites(onlyFavs);
// getOnlyMales(onlyMales);
// getOnlyFromEU(onlyMyCountry);
// getOnlyExperienced(onlyExperienced);

// task3
// sortByName(tableBody);
// sortByAge(tableBody);
// sortByCountryName(tableBody);

// task4
// findUser();

// task5
// addTeacher();


function pagination(topTeachers, modified){
  var btnContainer = document.querySelector(".pagination");
  // Get all buttons with class="btn" inside the container
  var btns = btnContainer.querySelectorAll('a');

for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName('active');
    current[0].className = current[0].className.replace("active", "");
    this.className += "active";
    printUsersOptional(topTeachers, modified, Number.parseInt(this.innerText));
    displayStatisticsTableOptional(tableBody, modified, Number.parseInt(this.innerText))
  });
}
}
