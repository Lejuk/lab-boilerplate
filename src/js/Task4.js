export function sortArray(array, value, descending) {
  let sorted;
  if (value === 'full_name' || value === 'country') {
    sorted = array.sort((a, b) => {
      const nameA = a[value].toLowerCase(); 
      const nameB = b[value].toLowerCase();
      if (nameA < nameB) { return -1; }
      if (nameA > nameB) { return 1; }
      return 0;
    });
  } else {
    sorted = array.sort((a, b) => a.age - b.age);
  }
  if (descending) { sorted.reverse(); }
  return sorted;
}
