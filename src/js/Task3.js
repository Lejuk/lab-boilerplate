export function filterArray(array, field, param) {
  const filtered = array.filter((item) => item[field] === param);
  if (filtered.length == 0) { console.log('There is no object with such parameters'); } else { return filtered; }
}
