export function validate(object) {
  if (validateStrings(object) && validateAge(object) && validatePhoneNumber(object) && validateEmail(object)) {
    // console.log('Validation has been passed successfully');
    return true;
  }

  // console.log('Validadion has been failed');
  return false;
}

function validateStrings(obj) {
  let check = true;
  const strings = ['full_name', 'title', 'note', 'state', 'city', 'country'];
  for (let i = 0; i < strings.length; i++) {
    if (typeof obj[strings[i]] === 'string' && obj[strings[i]][0].toUpperCase() === obj[strings[i]][0]) {
      check = true;
    } else {
      check = false;
      break;
    }
  }
  return check;
}

function validateAge(obj) {
  return typeof (obj.age) === 'number';
}

function validatePhoneNumber(obj) {
  const formats = [/^\S{3,12}$/, /^\d{2}-\d{2}-\d{2}-\d{2}-\d{2}$/, /^\d{4}-\d{7}$/, /^\(\d{3}\)-\d{3}-\d{4}$/];
  for (let i = 0; i < formats.length; i++) {
    if (obj.phone.match(formats[i])) {
      return true;
    }
    return false;
  }
}

function validateEmail(obj) {
  return !!obj.email.includes('@');
}
