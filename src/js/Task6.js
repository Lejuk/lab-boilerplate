export function percentageOf(array, value, operator) {
  let users;
  switch (operator) {
    case 'less':
      users = array.filter((user) => user.age < value);
      break;

    case 'greater':
      users = array.filter((user) => user.age > value);
      break;

    case 'equal':
      users = array.filter((user) => user.age === value);
      break;
    default:
      console.log("Oops, something went wrond, try to use 'less' 'greater' or 'equal' as operator");
  }
  const percentage = users.length / array.length * 100;
  return percentage;
}
